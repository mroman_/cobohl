**COBOHL** is (hopefully) going to be a new garbage collected
old-style stack-based programming language. 

```
REQUIRE std
REQUIRE io
ENTRYPOINT main

MODULE DIVISION
  NAME Hello World
  AUTHOR Roman Muentener
  TYPE EXECUTABLE
END DIVISION

DATA DIVISION
  GLOBAL Message
END DIVISION

INITIALIZATION DIVISION
  "Hello, world!" -> Message
END DIVISION

PROGRAM DIVISION
  FUNCTION main
  DEFINE    Message io:writeln-stdout io:exit-success
  DEFINED
END DIVISION
```

